# Accept a year input from the user and determine if it is a leap year or not.

year = int(input("Please enter a year:\n"))

if year%4 == 0 :
	print(f"{year} is a leap year!")
else :
	print(f"{year} is not a leap year!")

# Accept two numbers (row and col) from the user and create a grid of asterisks using the two numbers (row and col).

cols = int(input("Please enter number of columns: \n"))

rows = int(input("Please enter number of rows: \n"))

for x in range(rows):
    for y in range(cols):
        print('*',end = '')
    print()