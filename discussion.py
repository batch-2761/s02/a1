# Python also allows user to input, with this, users can give inputs to the program

# [Section] Input
# username = input("Please enter your name: \n")
# print(f"Hello {username}! Welcome to Python short course!")

# num1 = int(input("Enter first number:"))
# num2 = int(input("Enter second number:"))
# print(f"The sum of num1 and num2 is {num1+num2}")

# [SECTION] With user input, users can give inputs for the program to be used to control the application using control structures
# Control structures can be divided into selection and repetition control sturctures

# Selection  control structures allows the program to choose among choices and run specific codes depending on the choice taken(conditions)

# Repetition Control Structures allow the program to repeat certain blocks of code given a starting condition and termination condition

# [Section] If Else statements
# if-else statements are used to choose between two or more code blocks depending on the condition

# test_num = 75

# if test_num >= 60 :
# 	print("Test passed!")
# else :
# 	print("Test failed!")

# Note that in Python, curly braces ({}) are not needed to distinguish the code blocks inside the if or else block. Hence, indentations are important as python uses indentation to distinguish parts of code as needed

# [Section] if else chains can also be used to have more than 2 choices for the program

# test_num2 = int(input("Please enter the second number:\n"))

# if test_num2 > 0 :
# 	print("The number is positive!")
# 	print(f"The number provided is {test_num2}")
# elif test_num == 0 :
# 	print("The number is equal to 0!")
# else :
# 	print("The number is negative!")

# Mini Exercise 1:
# Create an if-else statement that determines if a number is divisible by 3, 5, or both. 
# If the number is divisible by 3, print "The number is divisible by 3"
# If the number is divisible by 5, print "The number is divisible by 5"
# If the number is divisible by 3 and 5, print "The number is divisible by both 3 and 5"
# If the number is not divisible by any, print "The number is not divisible by 3 nor 5"

# num = int(input("Please enter a number:\n"))

# if num%3 == 0 and num%5 == 0 :
# 	print("The number is divisible by 3 and 5!")
# elif num%3 == 0 :
# 	print("The number is divisible by 3!")
# elif num%5 == 0 :
# 	print("The number is divisible by 5!")
# else :
# 	print("The number is not divisible by 3 and 5!")

# [Section] Loops
# Python has loops that can repeat blocks of code
# While loops are used to execute a set of statements as long as the condition is true

# i = 0

# while i <= 5 :
# 	print(f"Current value of i is {i}")
# 	i += 1

# Result: 
# Current value of i is 0
# Current value of i is 1
# Current value of i is 2
# Current value of i is 3
# Current value of i is 4
# Current value of i is 5

# while i <= 5 :
# 	i += 1
# 	print(f"Current value of i is {i}")

# Result:
# Current value of i is 1
# Current value of i is 2
# Current value of i is 3
# Current value of i is 4
# Current value of i is 5
# Current value of i is 6


# [Section] For Loops are used for iteration over a sequence

# fruits = ["apple", "banana", "cherry"] #lists

# for indiv_fruit in fruits :
# 	print(indiv_fruit)

# Result:
# apple
# banana
# cherry


# [Section] range() method
# To use for loop to iterate through values, the range method can be used
#By the default, iterations in range method always starts with 0

# for x in range(6) :
# 	print(f"The current value of x is {x}")

# Result:
# Current value of i is 0
# Current value of i is 1
# Current value of i is 2
# Current value of i is 3
# Current value of i is 4
# Current value of i is 5


# for x in range(6, 10) :
# 	print(f"The current value of x is {x}")

# Result: 
# The current value of x is 6
# The current value of x is 7
# The current value of x is 8
# The current value of x is 9

# for x in range(6, 20, 2) :
# 	print(f"The current value of x is {x}")

# Result:
# The current value of x is 6
# The current value of x is 8
# The current value of x is 10
# The current value of x is 12
# The current value of x is 14
# The current value of x is 16

# [Section] Break Statement
# The break statement is used to stop the loop

# j = 1
# while j < 6 :
# 	print(j)

# 	if j==3 :
# 		break

# 	j+=1

# Result: 

# 1
# 2
# 3

# [Section] Continue Statement
# the continue statement returns the control to the beginning of the while loop and continue to the next iteration

k = 1
while k < 6 :
	k += 1

	if k==3 :
		continue

	print(k)

# Result :
# 2
# 4
# 5
# 6

# k = 1
# while k < 6 :
	

# 	if k==3 :
# 		continue

# 	k += 1
# 	print(k)

# Result :
# infinite loop